# MyPlace

MyPlace is Android app with option to save your favourite places based on your present location. You can name your place, add description and photo.

Places data is stored locally on your device in SQLite database. 
Images are not duplicated and only file path is stored, so in case of deleting previously attached photo default one is displayed.
User can additionally see his location on map.
All data associated with place can be modified, except location and date.

### Screenshots below:
#### Adding new place 
![image](shoots_MyPlace/device-2017-08-22-164701.png)
#### Displaying places
![image](shoots_MyPlace/device-2017-08-22-164552.png)
#### Editing place
![image](shoots_MyPlace/device-2017-08-22-162742.png)


To run:
Android Studio and/or some Android device with API 19+.
From 01.10.2017 you have to generate your own Google Maps API Key, as the one used in the app will be restricted.
All dependencies are included in gradle files.

Feel free to point out any mistakes or bad habits.
If using code for more than learning it would be nice if you leave me credit and inform about that.