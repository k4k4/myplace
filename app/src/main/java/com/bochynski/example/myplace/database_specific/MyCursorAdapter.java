package com.bochynski.example.myplace.database_specific;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bochynski.example.myplace.R;
import com.bochynski.example.myplace.database_specific.PlacesDbContract.PlacesEntry;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Karlo on 2017-08-15.
 */

public class MyCursorAdapter extends CursorAdapter {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

    public MyCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(final Context context, final Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.place_list_item, parent, false);
    }

    //cursor is already moved to correct position
    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        TextView nameView = (TextView) view.findViewById(R.id.place_name);
        TextView dateStampView = (TextView) view.findViewById(R.id.date_stamp);
        ImageView imageView = (ImageView) view.findViewById(R.id.card_image);

        String name = cursor.getString(cursor.getColumnIndex(PlacesEntry.COLUMN_NAME));
        long date = cursor.getLong(cursor.getColumnIndex(PlacesEntry.COLUMN_DATE));
        String imagePath = cursor.getString(cursor.getColumnIndex(PlacesEntry.COLUMN_IMAGE_PATH));

        try {
            File image = new File(imagePath);
            Picasso.with(context).load(image).fit().centerCrop().into(imageView);
        } catch (NullPointerException e) {
            Picasso.with(context).load(R.drawable.ic_location_default).fit().centerInside().into(imageView);
        }

        nameView.setText(name);
        dateStampView.setText(formatDate(date));
    }

    private String formatDate(long dateMilis) {
        Date d = new Date(dateMilis);
        return sdf.format(d);
    }
}
