package com.bochynski.example.myplace;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bochynski.example.myplace.database_specific.PlacesDAO;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class PlaceActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String ACTION_DATA_CHANGED = "data changed action";
    public static final String SAVED_KEY_LOCATION = "Saved location";
    public static final String SAVED_SHORT_NAME = "Saved short name";
    public static final String SAVED_DESCRIPTION = "Saved description";
    public static final String SAVED_IMAGE_PATH = "Saved image path";
    public static final String SAVED_DATE_MILIS = "Saved date milis";

    private static final String LOG_TAG = PlaceActivity.class.getSimpleName();
    private static final int PERMISSION_CODE_LOCATION = 107;
    private static final int REQUEST_CODE_GALLERY = 17;
    private static final int MARKER_COLOR = 230;
    private static final int MAP_ZOOM = 15;

    private BroadcastReceiver locationBroadcastReceiver;
    private AlertDialog dialog;
    private AlertDialog rationale;
    private Location location;
    private Snackbar snackbar;
    private GoogleMap googleMap;
    private String imagePath;
    private PlacesDAO placesDAO;
    private Marker current;
    private long dateMilis = -1;
    private int itemID = -1;
    private boolean changedSomething = false;

    @BindView(R.id.place_date_textView)
    TextView dateView;

    @BindView(R.id.place_add_image_button)
    ImageButton addImageButton;

    @BindView(R.id.place_imageView)
    ImageView imageView;

    @BindView(R.id.place_name)
    EditText placeNameEditText;

    @BindView(R.id.place_description)
    EditText placeDescriptionEditText;

    @BindView(R.id.place_map_view)
    MapView mapView;

    @BindView(R.id.gps_info_textView)
    TextView gpsUnavailableTextView;

    @BindView(R.id.place_progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.place_progress_action)
    TextView progressAction;

    @BindView(R.id.progress_layout)
    LinearLayout progressLayout;

    @BindView(android.R.id.content)
    View parentView;

    @SuppressWarnings("unused") // it is used, just not detected cause of injection
    @OnClick({R.id.place_add_image_button, R.id.place_imageView})
    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
    }

    @SuppressWarnings("unused") // it is used, just not detected cause of injection
    @OnTouch({R.id.place_description, R.id.place_name, R.id.place_add_image_button, R.id.place_imageView})
    public boolean changeMade() {
        changedSomething = true;
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);
        ButterKnife.bind(this);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        restoreData(savedInstanceState);
        initDate();
        if (isEditPlaceCall()) {
            setTitle(R.string.edit_place_activity_title);
            invalidateOptionsMenu();
        }
        if (location == null) {
            if (!needToAskForPermission()) {
                checkProviderEnabled();
                startServiceGPS();
            }
        } else {
            showMapsView();
        }
    }

    private boolean isEditPlaceCall() {
        Bundle bundle = getIntent().getExtras();
        boolean isEditCall = false;
        if (bundle != null) {
            if (bundle.containsKey(MainActivity.EDIT_ITEM_CALL_KEY)) {
                itemID = bundle.getInt(MainActivity.EDIT_ITEM_CALL_KEY);
                isEditCall = true;
                restoreData(bundle);
            }
        }
        return isEditCall;
    }

    private void restoreData(Bundle bundle) {
        if (bundle != null) {
            location = bundle.getParcelable(SAVED_KEY_LOCATION);
            String shortName = bundle.getString(SAVED_SHORT_NAME);
            String description = bundle.getString(SAVED_DESCRIPTION);
            imagePath = bundle.getString(SAVED_IMAGE_PATH);
            itemID = bundle.getInt(MainActivity.EDIT_ITEM_CALL_KEY);
            dateMilis = bundle.getLong(SAVED_DATE_MILIS);
            placeNameEditText.setText(shortName);
            placeDescriptionEditText.setText(description);
            if (imagePath != null) {
                setLocationImage(imagePath);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        if (locationBroadcastReceiver == null) {
            locationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action == ServiceGPS.ACTION_LOCATION_UPDATE) {
                        //received location there
                        location = intent.getParcelableExtra(ServiceGPS.EXTRA_KEY_LOCATION);
                        setUpMapView();
                        stopServiceGPS();
                    } else if (action == ServiceGPS.ACTION_LOCATION_DISABLED) {
                        showSnackBarLocationRequired();
                    } else if (action == ServiceGPS.ACTION_LOCATION_ENABLED) {
                        showProgress();
                        dismissPrompters();
                    }
                }
            };
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ServiceGPS.ACTION_LOCATION_UPDATE);
        filter.addAction(ServiceGPS.ACTION_LOCATION_DISABLED);
        filter.addAction(ServiceGPS.ACTION_LOCATION_ENABLED);
        registerReceiver(locationBroadcastReceiver, filter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_KEY_LOCATION, location);
        outState.putString(SAVED_SHORT_NAME, placeNameEditText.getText().toString());
        outState.putString(SAVED_DESCRIPTION, placeDescriptionEditText.getText().toString());
        outState.putString(SAVED_IMAGE_PATH, imagePath);
        outState.putInt(MainActivity.EDIT_ITEM_CALL_KEY, itemID);
        outState.putLong(SAVED_DATE_MILIS, dateMilis);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem itemToHide = menu.findItem(R.id.appbar_delete);
        if (itemID == -1) {
            itemToHide.setVisible(false);
        } else {
            itemToHide.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_add_place, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.appbar_save:
                if (location != null) {
                    if (itemID == -1) {
                        saveNewPlace();
                    } else {
                        updatePlace();
                    }
                } else {
                    showToast(R.string.cant_save_no_location);
                }
                handled = true;
                break;
            case R.id.appbar_delete:
                deletePlace();
                handled = true;
                break;
            case android.R.id.home:
                handled = true;
                if (!changedSomething) {
                    NavUtils.navigateUpFromSameTask(this);
                } else {
                    DialogInterface.OnClickListener discardButtonClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            NavUtils.navigateUpFromSameTask(PlaceActivity.this);
                        }
                    };
                    showUnsavedChangesDialog(discardButtonClickListener);
                }
                break;
        }
        return handled ? true : super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (changedSomething) {
            DialogInterface.OnClickListener discardButtonClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            };
            showUnsavedChangesDialog(discardButtonClickListener);
        } else {
            super.onBackPressed();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationBroadcastReceiver != null) {
            unregisterReceiver(locationBroadcastReceiver);
        }
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == REQUEST_CODE_GALLERY)
            if (data != null) {
                Uri contentURI = data.getData();
                if (contentURI != null) {
                    imagePath = getPath(contentURI);
                    setLocationImage(imagePath);
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CODE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    startServiceGPS();
                    checkProviderEnabled();
                } else if ((grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) && Build.VERSION.SDK_INT >= 23) {
                    boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]) || shouldShowRequestPermissionRationale(permissions[1]);
                    if (showRationale) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        if (rationale == null || !rationale.isShowing()) {
                            rationale = builder.setMessage(R.string.location_rationale).setTitle(R.string.location_rationale_title).setCancelable(false).
                                    setPositiveButton(R.string.got_it, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            needToAskForPermission();
                                        }
                                    }).create();
                            rationale.show();
                        }
                    }
                } else {
                    needToAskForPermission();
                }
                break;
        }
    }

    public boolean needToAskForPermission() {
        boolean needToAsk = false;
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE_LOCATION);
            showGpsUnavailableTextView();
            needToAsk = true;
        }
        return needToAsk;
    }

    private void checkProviderEnabled() {
        LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGPS();
        }
    }

    private void startServiceGPS() {
        Intent intent = new Intent(getApplicationContext(), ServiceGPS.class);
        startService(intent);
    }

    private void stopServiceGPS() {
        Intent intent = new Intent(getApplicationContext(), ServiceGPS.class);
        stopService(intent);
    }

    private void buildAlertMessageNoGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.no_gps_want_to_enable).setCancelable(false).setIcon(R.drawable.ic_gps)
                .setTitle(R.string.title_alert_gps).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                redirectUserToLocationSettings();
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                showSnackBarLocationRequired();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void showSnackBarLocationRequired() {
        showGpsUnavailableTextView();
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        snackbar = Snackbar.make(parentView, R.string.gps_is_required, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.got_it, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectUserToLocationSettings();
            }
        });
        snackbar.show();
    }

    private void redirectUserToLocationSettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void setUpMapView() {
        refreshMapMarker();
        showMapsView();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng coordinates;
        if (location == null) {
            coordinates = new LatLng(0, 0);
        } else {
            coordinates = new LatLng(location.getLatitude(), location.getLongitude());
        }
        googleMap = map;
        if (current != null) {
            current.remove();
            current = null;
        }
        current = googleMap.addMarker(new MarkerOptions().position(coordinates).icon(BitmapDescriptorFactory.defaultMarker(MARKER_COLOR)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, MAP_ZOOM));
        mapView.onResume();
    }

    private void refreshMapMarker() {
        if (location != null && googleMap != null) {
            LatLng coordinates = new LatLng(location.getLatitude(), location.getLongitude());
            Log.v(LOG_TAG, "coordinates: " + coordinates.latitude + " " + coordinates.longitude);
            if (current != null) {
                current.remove();
                current = null;
            }
            current = googleMap.addMarker(new MarkerOptions().position(coordinates).icon(BitmapDescriptorFactory.defaultMarker(MARKER_COLOR)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, MAP_ZOOM));
            mapView.onResume();
        }
    }

    private void showMapsView() {
        hideProgressViews();
        gpsUnavailableTextView.setVisibility(View.GONE);
        mapView.setVisibility(View.VISIBLE);
    }

    private void showGpsUnavailableTextView() {
        hideProgressViews();
        mapView.setVisibility(View.GONE);
        gpsUnavailableTextView.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        mapView.setVisibility(View.GONE);
        gpsUnavailableTextView.setVisibility(View.GONE);
        showProgressViews();
    }

    private void showProgressViews() {
        progressLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressAction.setVisibility(View.VISIBLE);
    }

    private void hideProgressViews() {
        progressLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        progressAction.setVisibility(View.GONE);
    }

    private void dismissPrompters() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if (rationale != null && rationale.isShowing()) {
            rationale.dismiss();
        }
    }

    private void setLocationImage(String imagePath) {
        File image = new File(imagePath);
        if (image != null) {
            Picasso.with(this).load(image).fit().centerCrop().into(imageView);
            addImageButton.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    private String getPath(Uri contentUri) {
        String res = null;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private void showUnsavedChangesDialog(DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_message);
        builder.setPositiveButton(R.string.discard_changes, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void initDate() {
        Date d;
        if (dateMilis == -1) {
            d = Calendar.getInstance().getTime();
            dateMilis = d.getTime();
        } else {
            d = new Date(dateMilis);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        String dateFormatted = sdf.format(d);
        dateView.setText(dateFormatted);
    }

    private void initPlaceDAO() {
        if (placesDAO == null) {
            placesDAO = new PlacesDAO(this);
        }
    }

    private void saveNewPlace() {
        initPlaceDAO();
        boolean successful = placesDAO.addNewPlace(
                location,
                placeNameEditText.getEditableText().toString(),
                placeDescriptionEditText.getEditableText().toString(),
                dateMilis,
                imagePath);
        if (successful) {
            showToast(R.string.successful_save);
            sendBroadcastDataChanged();
            changedSomething = false;
            finish();
        } else {
            showToast(R.string.error_while_saving);
        }
    }

    private void updatePlace() {
        initPlaceDAO();
        boolean successful = placesDAO.modifyPlace(
                itemID,
                placeNameEditText.getEditableText().toString(),
                placeDescriptionEditText.getEditableText().toString(),
                imagePath);
        if (successful) {
            showToast(R.string.successful_update);
            sendBroadcastDataChanged();
            changedSomething = false;
        } else {
            showToast(R.string.error_while_update);
        }
    }

    private void deletePlace() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_text);
        builder.setTitle(R.string.delete_dialog_title);
        builder.setPositiveButton(R.string.delete, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                initPlaceDAO();
                boolean successful = placesDAO.deletePlace(itemID);
                if (successful) {
                    showToast(R.string.successful_delete);
                    sendBroadcastDataChanged();
                    changedSomething = false;
                    finish();
                } else {
                    showToast(R.string.error_while_delete);
                }
            }
        });
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showToast(int stringResource) {
        Toast.makeText(this, stringResource, Toast.LENGTH_SHORT).show();
    }

    private void sendBroadcastDataChanged() {
        Intent intent = new Intent(ACTION_DATA_CHANGED);
        sendBroadcast(intent);
    }
}
