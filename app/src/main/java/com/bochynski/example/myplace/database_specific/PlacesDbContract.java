package com.bochynski.example.myplace.database_specific;

import android.provider.BaseColumns;

/**
 * Created by Karlo on 2017-08-15.
 */

public class PlacesDbContract {

    private PlacesDbContract() {

    }

    public static abstract class PlacesEntry implements BaseColumns {
        public static final String TABLE_NAME = "places";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_DATE = "date_unix";
        public static final String COLUMN_IMAGE_PATH = "image_path";
    }

}
