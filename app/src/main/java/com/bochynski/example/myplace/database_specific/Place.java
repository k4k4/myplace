package com.bochynski.example.myplace.database_specific;

/**
 * Created by Karlo on 2017-08-15.
 */

public class Place {

    private int _id;
    private double latitude;
    private double longitude;
    private String name;
    private String description;
    private int dateUnix;
    private String imagePath;

    public Place(int _id, double latitude, double longitude, String name, String description, int dateUnix, String imagePath) {
        this._id = _id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.description = description;
        this.dateUnix = dateUnix;
        this.imagePath = imagePath;
    }

    public int get_id() {
        return _id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDateUnix() {
        return dateUnix;
    }

    public void setDateUnix(int dateUnix) {
        this.dateUnix = dateUnix;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
