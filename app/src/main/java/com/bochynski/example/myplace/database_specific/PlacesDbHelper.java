package com.bochynski.example.myplace.database_specific;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bochynski.example.myplace.database_specific.PlacesDbContract.PlacesEntry;

/**
 * Created by Karlo on 2017-08-15.
 */

public class PlacesDbHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "places_db.db";
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE IF NOT EXISTS " +
            PlacesEntry.TABLE_NAME + "(" +
            PlacesEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            PlacesEntry.COLUMN_LATITUDE + " REAL NOT NULL," +
            PlacesEntry.COLUMN_LONGITUDE + " REAL NOT NULL," +
            PlacesEntry.COLUMN_NAME + " TEXT," +
            PlacesEntry.COLUMN_DESCRIPTION + " TEXT," +
            PlacesEntry.COLUMN_DATE + " INTEGER ," +
            PlacesEntry.COLUMN_IMAGE_PATH + " TEXT" +
            ");";
    private static final String SQL_DROP_ENTRIES = "DROP TABLE IF EXISTS " + PlacesEntry.TABLE_NAME;

    public PlacesDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //implement properly when some changes to db schema made in released app
        db.execSQL(SQL_DROP_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //implement properly when some changes to db schema made in released app
        db.execSQL(SQL_DROP_ENTRIES);
        onCreate(db);
    }

    @Override
    public String getDatabaseName() {
        return DB_NAME;
    }
}
