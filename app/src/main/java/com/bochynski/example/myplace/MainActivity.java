package com.bochynski.example.myplace;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bochynski.example.myplace.database_specific.MyCursorAdapter;
import com.bochynski.example.myplace.database_specific.PlacesCursorLoader;
import com.bochynski.example.myplace.database_specific.PlacesDbContract.PlacesEntry;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String EDIT_ITEM_CALL_KEY = "Edit call";

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private MyCursorAdapter adapter;
    private int sortByDateFlag, sortByNameFlag;
    private Cursor cursor;
    private BroadcastReceiver receiver;
    private MenuItem sortDateItem, sortNameItem;

    @BindView(R.id.list_places)
    ListView list;
    @BindView(R.id.fab_add_place)
    FloatingActionButton fabAddPlace;

    @OnClick(R.id.fab_add_place)
    public void addNewPlace() {
        Intent i = new Intent(this, PlaceActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sortByDateFlag = 0;
        sortByNameFlag = 0;
        adapter = new MyCursorAdapter(this, null);
        setUpListView();
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_main, menu);
        sortDateItem = menu.findItem(R.id.appbar_sort_date);
        sortNameItem = menu.findItem(R.id.appbar_sort_name);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.appbar_sort_date:
                sortByDate();
                break;
            case R.id.appbar_sort_name:
                sortByName();
                break;
        }
        return handled;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (receiver == null) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action == PlaceActivity.ACTION_DATA_CHANGED) {
                        getSupportLoaderManager().restartLoader(-1, null, MainActivity.this);
                    }
                }
            };
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(PlaceActivity.ACTION_DATA_CHANGED);
        registerReceiver(receiver, filter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.v(LOG_TAG, "onCreateLoader");
        return new PlacesCursorLoader(this, id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.v(LOG_TAG, "onLoadFinished");
        cursor = data;
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    private void setUpListView() {
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (cursor != null) {
                    cursor.moveToPosition(position);
                    String name = cursor.getString(cursor.getColumnIndex(PlacesEntry.COLUMN_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(PlacesEntry.COLUMN_DESCRIPTION));
                    String imagePath = cursor.getString(cursor.getColumnIndex(PlacesEntry.COLUMN_IMAGE_PATH));
                    long dateMilis = cursor.getLong(cursor.getColumnIndex(PlacesEntry.COLUMN_DATE));
                    double latitude = cursor.getDouble(cursor.getColumnIndex(PlacesEntry.COLUMN_LATITUDE));
                    double longitude = cursor.getDouble(cursor.getColumnIndex(PlacesEntry.COLUMN_LONGITUDE));
                    startEditPlaceActivity((int) id, name, description, imagePath, dateMilis, latitude, longitude);
                }
            }
        });
    }

    private void sortByDate() {
        switch (sortByDateFlag) {
            case 0:
                sortByDateAscending();
                break;
            case 1:
                sortByDateDescending();
                break;
            case 2:
                bringBackDefaultOrder();
                sortDateItem.setTitle(R.string.sort_date_item);
                break;
            default:
                resetSortingByDate();
        }
        sortByDateFlag = (sortByDateFlag + 1) % 3;
        if (sortByNameFlag != 0) {
            resetSortingByName(); // only one way of sorting enabled at a time
        }
    }

    private void resetSortingByDate() {
        sortByDateFlag = 0;
        sortDateItem.setTitle(R.string.sort_date_item);
    }

    private void sortByDateAscending() {
        getSupportLoaderManager().restartLoader(PlacesCursorLoader.DATE_ASCENDING, null, this);
        sortDateItem.setTitle(R.string.sort_date_ascending);
    }

    private void sortByDateDescending() {
        getSupportLoaderManager().restartLoader(PlacesCursorLoader.DATE_DESCENDING, null, this);
        sortDateItem.setTitle(R.string.sort_date_descending);
    }

    private void sortByName() {
        switch (sortByNameFlag) {
            case 0:
                sortByNameAscending();
                break;
            case 1:
                sortByNameDescending();
                break;
            case 2:
                bringBackDefaultOrder();
                sortNameItem.setTitle(R.string.sort_name_item);
                break;
            default:
                resetSortingByName();
        }
        sortByNameFlag = (sortByNameFlag + 1) % 3;
        if (sortByDateFlag != 0) {
            resetSortingByDate();
        }
    }

    private void resetSortingByName() {
        sortByNameFlag = 0;
        sortNameItem.setTitle(R.string.sort_name_item);
    }

    private void sortByNameAscending() {
        getSupportLoaderManager().restartLoader(PlacesCursorLoader.NAME_ASCENDING, null, this);
        sortNameItem.setTitle(R.string.sort_name_ascending);
    }

    private void sortByNameDescending() {
        getSupportLoaderManager().restartLoader(PlacesCursorLoader.NAME_DESCENDING, null, this);
        sortNameItem.setTitle(R.string.sort_name_descending);
    }

    private void bringBackDefaultOrder() {
        getSupportLoaderManager().restartLoader(0, null, this);
    }

    private void startEditPlaceActivity(int itemID, String name, String description, String imagePath, long dateMilis, double latitude, double longitude) {
        Bundle data = new Bundle();
        data.putInt(EDIT_ITEM_CALL_KEY, itemID);
        data.putString(PlaceActivity.SAVED_SHORT_NAME, name);
        data.putString(PlaceActivity.SAVED_DESCRIPTION, description);
        data.putString(PlaceActivity.SAVED_IMAGE_PATH, imagePath);
        data.putLong(PlaceActivity.SAVED_DATE_MILIS, dateMilis);
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        data.putParcelable(PlaceActivity.SAVED_KEY_LOCATION, location);
        Intent intent = new Intent(this, PlaceActivity.class);
        intent.putExtras(data);
        startActivity(intent);
    }
}
