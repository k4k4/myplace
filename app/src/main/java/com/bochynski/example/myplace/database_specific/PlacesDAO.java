package com.bochynski.example.myplace.database_specific;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.location.Location;

import com.bochynski.example.myplace.database_specific.PlacesDbContract.PlacesEntry;

/**
 * Created by Karlo on 2017-08-15.
 */

public class PlacesDAO {

    private SQLiteOpenHelper dbHelper;
    private static final String ASCENDING = " ASC";
    private static final String DESCENDING = " DESC";
    private static final String IS_EQUAL_PARAM = "=?";

    public PlacesDAO(Context context) {
        dbHelper = new PlacesDbHelper(context);
    }

    public Cursor getAllPlaces() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PlacesDbContract.PlacesEntry.TABLE_NAME);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = builder.query(db, null, null, null, null, null, null);
        return cursor;
    }

    /*
     * from the oldest to the newest
     */
    public Cursor getPlacesSortByDateAscending() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PlacesDbContract.PlacesEntry.TABLE_NAME);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = builder.query(db, null, null, null, null, null, PlacesDbContract.PlacesEntry.COLUMN_DATE + ASCENDING);
        return cursor;
    }

    /*
     * starting from the newest to the oldest
     */
    public Cursor getPlacesSortByDateDescending() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PlacesDbContract.PlacesEntry.TABLE_NAME);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = builder.query(db, null, null, null, null, null, PlacesDbContract.PlacesEntry.COLUMN_DATE + DESCENDING);
        return cursor;
    }

    /*
     * A - Z
     */
    public Cursor getPlacesSortByNameAscending() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PlacesDbContract.PlacesEntry.TABLE_NAME);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = builder.query(db, null, null, null, null, null, PlacesDbContract.PlacesEntry.COLUMN_NAME + ASCENDING);
        return cursor;
    }

    /*
     * Z - A
     */
    public Cursor getPlacesSortByNameDescending() {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(PlacesDbContract.PlacesEntry.TABLE_NAME);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = builder.query(db, null, null, null, null, null, PlacesDbContract.PlacesEntry.COLUMN_NAME + DESCENDING);
        return cursor;
    }

    public boolean addNewPlace(Location location, String name, String description, long date, String pathImage) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PlacesEntry.COLUMN_LATITUDE, latitude);
        values.put(PlacesEntry.COLUMN_LONGITUDE, longitude);
        values.put(PlacesEntry.COLUMN_NAME, name);
        values.put(PlacesEntry.COLUMN_DESCRIPTION, description);
        values.put(PlacesEntry.COLUMN_DATE, date);
        values.put(PlacesEntry.COLUMN_IMAGE_PATH, pathImage);
        boolean success = true;
        try {
            db.insertOrThrow(PlacesEntry.TABLE_NAME, null, values);
        } catch (SQLException e) {
            success = false;
        }
        db.close();
        return success;
    }

    /*
     * date and location of place remain the same, not editable
     */
    public boolean modifyPlace(int itemID, String name, String description, String pathImage) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PlacesEntry.COLUMN_NAME, name);
        values.put(PlacesEntry.COLUMN_DESCRIPTION, description);
        values.put(PlacesEntry.COLUMN_IMAGE_PATH, pathImage);
        int rowsAffected = db.update(PlacesEntry.TABLE_NAME, values, PlacesEntry.COLUMN_ID + IS_EQUAL_PARAM, new String[]{String.valueOf(itemID)});
        db.close();
        boolean successful = true;
        if (rowsAffected != 1) { // should be 1 as itemID s planned to be unique for given item
            successful = false;
        }
        return successful;
    }

    public boolean deletePlace(int itemID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsAffected = db.delete(PlacesEntry.TABLE_NAME, PlacesEntry.COLUMN_ID + IS_EQUAL_PARAM, new String[]{String.valueOf(itemID)});
        db.close();
        boolean successful = true;
        if (rowsAffected != 1) { // should be 1 as itemID s planned to be unique for given item
            successful = false;
        }
        return successful;
    }

}
