package com.bochynski.example.myplace;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;

/**
 * Created by Karlo on 2017-08-17.
 */

// warnings suppressed as we handle them in PlaceActivity
@SuppressWarnings("MissingPermission")
public class ServiceGPS extends Service {

    public static final String ACTION_LOCATION_UPDATE = "com.bochynski.ACTION_LOCATION_UPDATE";
    public static final String ACTION_LOCATION_DISABLED = "com.bochynski.ACTION_LOCATION_DISABLED";
    public static final String ACTION_LOCATION_ENABLED = "com.bochynski.ACTION_LOCATION_ENABLED";
    public static final String EXTRA_KEY_LOCATION = "location key";

    private LocationListener locationListener;
    private LocationManager locationManager;
    private String[] providers = new String[]{
            LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER
    };
    private static final int MIN_TIME = 2000;
    private static final int MIN_DISTANCE = 100;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        setLocationListener();
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        requestUpdates();
    }

    private void setLocationListener() {
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Intent intent = new Intent(ACTION_LOCATION_UPDATE);
                intent.putExtra(EXTRA_KEY_LOCATION, location);
                sendBroadcast(intent);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                requestUpdates();
                Intent intent = new Intent(ACTION_LOCATION_ENABLED);
                sendBroadcast(intent);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(ACTION_LOCATION_DISABLED);
                sendBroadcast(intent);
            }
        };
    }

    private void requestUpdates() {
        for (String s : providers) {
            locationManager.requestLocationUpdates(s, MIN_TIME, MIN_DISTANCE, locationListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }
}
