package com.bochynski.example.myplace.database_specific;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by Karlo on 2017-08-15.
 */

public class PlacesCursorLoader extends SimpleCursorLoader {

    public static final int DATE_ASCENDING = 11;
    public static final int DATE_DESCENDING = 12;
    public static final int NAME_ASCENDING = 33;
    public static final int NAME_DESCENDING = 34;
    private PlacesDAO dao;
    private Context context;
    private int flag;

    public PlacesCursorLoader(Context context, int flag) {
        super(context);
        this.context = context;
        this.flag = flag;
    }

    @Override
    public Cursor loadInBackground() {
        dao = new PlacesDAO(context);
        Cursor cursor;
        switch (flag) {
            case DATE_ASCENDING:
                cursor = dao.getPlacesSortByDateAscending();
                break;
            case DATE_DESCENDING:
                cursor = dao.getPlacesSortByDateDescending();
                break;
            case NAME_ASCENDING:
                cursor = dao.getPlacesSortByNameAscending();
                break;
            case NAME_DESCENDING:
                cursor = dao.getPlacesSortByNameDescending();
                break;
            default:
                cursor = dao.getAllPlaces();
        }
        return cursor;
    }
}
